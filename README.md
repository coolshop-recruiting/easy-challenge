# Easy Challenge
Write a program that given the name of a CSV file as input from the command line, 
containing the list of orders of an ecommerce site, prints in output the line with 
the highest total import, the one with the highest quantity and the one with the greatest difference
between total without discount and total with discount		

Bonus point: candidate writes unit tests

## Input
The input file is a CSV file with the following structure:

| Id | Article Name | Quantity | Unit Price | Percentage Discount | Buyer |
|-----|--------------|----------|------------|---------------------|------|
|1|Coke|10|1|0|Mario Rossi|
|2|Coke|15|2|0|Luca Neri|
|3|Fanta|5|3|2|Luca Neri|
|4|Water|20|1|10|Mario Rossi|
|5|Fanta|1|4|15|Andrea Bianchi|


		
		
		
		


